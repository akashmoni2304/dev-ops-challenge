package com.gameservice.turn.constant;

public interface EnumConstants {

    enum SUITS {
        SPADE, HEART, DIAMOND, CLUB
    }

    enum TURN_STATUS {
        ACTIVE, INACTIVE
    }

}
