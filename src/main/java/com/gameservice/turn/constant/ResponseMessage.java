package com.gameservice.turn.constant;

public interface ResponseMessage {

    String TURN_DECIDE_ERROR = "Unable to decide turn player.";

    String PARTICIPANT_NOT_FOUND_ERROR = "Unable to find participant.";

}
