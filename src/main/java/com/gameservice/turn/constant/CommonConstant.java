package com.gameservice.turn.constant;

public interface CommonConstant {

    int TOSS_DECK = 1;

    int TURN_TIMER = 30;

    int EXTRA_TURN_TIMER = 15;

    int DISCONNECTED_TURN_TIMER = 3;

    int MELDING_TIMER = 15;

    int TURN_DISCONNECTED_DROP = 21;

}
