package com.gameservice.outcome.repository;

import com.gameservice.outcome.model.RejoinModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RejoinRepository extends JpaRepository<RejoinModel, Long> {
}
