package com.gameservice.outcome.service;

import com.gameservice.outcome.dto.PageRequestDto;

public interface ReportService {

    Long getTotalRake(PageRequestDto pageRequestDto);

}
