package com.gameservice.outcome.constant;

public interface CashierServiceEndpoints {

    String BASE_URL = "http://localhost:3003";

    String GET_BALANCE_URL = BASE_URL + "/balance/";

    String UPDATE_BALANCE_URL = BASE_URL + "/balance";


}
