package com.gameservice.outcome.dto;

import lombok.Data;

@Data
public class DealsDetailsDto {

    private Long id;
    private Long gameId;
    private Integer wildJoker;

}
