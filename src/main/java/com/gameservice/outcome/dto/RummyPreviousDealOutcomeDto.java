package com.gameservice.outcome.dto;

import lombok.Data;

@Data
public class RummyPreviousDealOutcomeDto {

    private long playerId;
    private int totalScore;

}
