package com.gameservice.outcome.dto;

import lombok.Data;

@Data
public class DealsSaveDto {

    private Long gameId;
    private Integer wildJoker;

}
