package com.gameservice.outcome.dto;

import lombok.Data;

@Data
public class RoomCreateDto {

    private String roomName;

}
