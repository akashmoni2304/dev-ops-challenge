package com.gameservice.distribution.dto;

import lombok.Data;

@Data
public class ParticipantsDto {

    private Long participantId;
    private Long playerId;

}
